-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 05. Jun 2017 um 17:25
-- Server-Version: 10.1.21-MariaDB
-- PHP-Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `webtechproj`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `fgenre`
--

CREATE TABLE `fgenre` (
  `g_id` int(11) NOT NULL,
  `f_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `fgenre`
--

INSERT INTO `fgenre` (`g_id`, `f_id`) VALUES
(1, 1),
(2, 1),
(3, 2),
(3, 3),
(4, 3),
(4, 4);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `filme`
--

CREATE TABLE `filme` (
  `f_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `jahr` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `filme`
--

INSERT INTO `filme` (`f_id`, `name`, `jahr`) VALUES
(1, 'Braindead', 1992),
(2, 'Meet the Feebles', 1989),
(3, 'Pain and Gain', 2013),
(4, 'Bad Boys 2', 2003);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `genre`
--

CREATE TABLE `genre` (
  `g_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `genre`
--

INSERT INTO `genre` (`g_id`, `name`) VALUES
(1, 'Splatter'),
(2, 'Horror'),
(3, 'Komödie'),
(4, 'Action');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `regifilme`
--

CREATE TABLE `regifilme` (
  `reg_id` int(11) DEFAULT NULL,
  `f_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `regifilme`
--

INSERT INTO `regifilme` (`reg_id`, `f_id`) VALUES
(1, 1),
(1, 2),
(2, 3),
(2, 4);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `regisseur`
--

CREATE TABLE `regisseur` (
  `reg_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `vorname` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `regisseur`
--

INSERT INTO `regisseur` (`reg_id`, `name`, `vorname`) VALUES
(1, 'Jackson', 'Peter'),
(2, 'Bay', 'Michael');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `scfilme`
--

CREATE TABLE `scfilme` (
  `sc_id` int(11) DEFAULT NULL,
  `f_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `scfilme`
--

INSERT INTO `scfilme` (`sc_id`, `f_id`) VALUES
(1, 1),
(2, 1),
(3, 2),
(4, 2),
(5, 3),
(6, 3),
(7, 4),
(8, 4);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `schauspieler`
--

CREATE TABLE `schauspieler` (
  `sc_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `vorname` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `schauspieler`
--

INSERT INTO `schauspieler` (`sc_id`, `name`, `vorname`) VALUES
(1, 'Balme', 'Timothy'),
(2, 'Peñalver', 'Diana'),
(3, 'Akersten', 'Donna'),
(4, 'Devenie', 'Stuart'),
(5, 'Wahlberg', 'Mark'),
(6, 'Johnson', 'Dwayne'),
(7, 'Smith', 'Will'),
(8, 'Lawrence', 'Martin');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `umovie`
--

CREATE TABLE `umovie` (
  `f_id` int(11) DEFAULT NULL,
  `u_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `umovie`
--

INSERT INTO `umovie` (`f_id`, `u_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `userdata`
--

CREATE TABLE `userdata` (
  `u_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `passwort` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `userdata`
--

INSERT INTO `userdata` (`u_id`, `name`, `passwort`) VALUES
(1, 'stm', 'CBD');

-- --------------------------------------------------------

--
-- Stellvertreter-Struktur des Views `v_film_genre`
-- (Siehe unten für die tatsächliche Ansicht)
--
CREATE TABLE `v_film_genre` (
`g_id` int(11)
,`Genre_Name` varchar(255)
,`Film_Name` varchar(255)
,`Erscheinungs_Jahr` int(11)
);

-- --------------------------------------------------------

--
-- Stellvertreter-Struktur des Views `v_regi_filme`
-- (Siehe unten für die tatsächliche Ansicht)
--
CREATE TABLE `v_regi_filme` (
`reg_id` int(11)
,`vorname` varchar(255)
,`name` varchar(255)
,`Film_Name` varchar(255)
,`Erscheinungs_Jahr` int(11)
);

-- --------------------------------------------------------

--
-- Stellvertreter-Struktur des Views `v_schauspieler_filme`
-- (Siehe unten für die tatsächliche Ansicht)
--
CREATE TABLE `v_schauspieler_filme` (
`sc_id` int(11)
,`Vorname` varchar(255)
,`Nachname` varchar(255)
,`Film_Name` varchar(255)
,`Erscheinungs_Jahr` int(11)
);

-- --------------------------------------------------------

--
-- Stellvertreter-Struktur des Views `v_user_movie`
-- (Siehe unten für die tatsächliche Ansicht)
--
CREATE TABLE `v_user_movie` (
`u_id` int(11)
,`Film_Name` varchar(255)
,`Erscheinungs_Jahr` int(11)
);

-- --------------------------------------------------------

--
-- Struktur des Views `v_film_genre`
--
DROP TABLE IF EXISTS `v_film_genre`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_film_genre`  AS  select `genre`.`g_id` AS `g_id`,`genre`.`name` AS `Genre_Name`,`filme`.`name` AS `Film_Name`,`filme`.`jahr` AS `Erscheinungs_Jahr` from ((`fgenre` join `genre` on((`fgenre`.`g_id` = `genre`.`g_id`))) join `filme` on((`filme`.`f_id` = `fgenre`.`f_id`))) order by `genre`.`g_id` ;

-- --------------------------------------------------------

--
-- Struktur des Views `v_regi_filme`
--
DROP TABLE IF EXISTS `v_regi_filme`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_regi_filme`  AS  select `regifilme`.`reg_id` AS `reg_id`,`regisseur`.`vorname` AS `vorname`,`regisseur`.`name` AS `name`,`filme`.`name` AS `Film_Name`,`filme`.`jahr` AS `Erscheinungs_Jahr` from ((`regisseur` join `regifilme` on((`regifilme`.`reg_id` = `regisseur`.`reg_id`))) join `filme` on((`regifilme`.`f_id` = `filme`.`f_id`))) order by `regifilme`.`reg_id` ;

-- --------------------------------------------------------

--
-- Struktur des Views `v_schauspieler_filme`
--
DROP TABLE IF EXISTS `v_schauspieler_filme`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_schauspieler_filme`  AS  select `scfilme`.`sc_id` AS `sc_id`,`schauspieler`.`vorname` AS `Vorname`,`schauspieler`.`name` AS `Nachname`,`filme`.`name` AS `Film_Name`,`filme`.`jahr` AS `Erscheinungs_Jahr` from ((`schauspieler` join `scfilme` on((`scfilme`.`sc_id` = `schauspieler`.`sc_id`))) join `filme` on((`scfilme`.`f_id` = `filme`.`f_id`))) order by `scfilme`.`sc_id` ;

-- --------------------------------------------------------

--
-- Struktur des Views `v_user_movie`
--
DROP TABLE IF EXISTS `v_user_movie`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_user_movie`  AS  select `umovie`.`u_id` AS `u_id`,`filme`.`name` AS `Film_Name`,`filme`.`jahr` AS `Erscheinungs_Jahr` from (`umovie` join `filme` on((`umovie`.`f_id` = `filme`.`f_id`))) order by `umovie`.`u_id` ;

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `fgenre`
--
ALTER TABLE `fgenre`
  ADD PRIMARY KEY (`g_id`,`f_id`),
  ADD KEY `film` (`f_id`);

--
-- Indizes für die Tabelle `filme`
--
ALTER TABLE `filme`
  ADD PRIMARY KEY (`f_id`);

--
-- Indizes für die Tabelle `genre`
--
ALTER TABLE `genre`
  ADD PRIMARY KEY (`g_id`);

--
-- Indizes für die Tabelle `regifilme`
--
ALTER TABLE `regifilme`
  ADD KEY `reg_id` (`reg_id`),
  ADD KEY `f_id` (`f_id`);

--
-- Indizes für die Tabelle `regisseur`
--
ALTER TABLE `regisseur`
  ADD PRIMARY KEY (`reg_id`);

--
-- Indizes für die Tabelle `scfilme`
--
ALTER TABLE `scfilme`
  ADD KEY `f_id` (`f_id`),
  ADD KEY `sc_id` (`sc_id`);

--
-- Indizes für die Tabelle `schauspieler`
--
ALTER TABLE `schauspieler`
  ADD PRIMARY KEY (`sc_id`);

--
-- Indizes für die Tabelle `umovie`
--
ALTER TABLE `umovie`
  ADD KEY `f_id` (`f_id`),
  ADD KEY `u_id` (`u_id`);

--
-- Indizes für die Tabelle `userdata`
--
ALTER TABLE `userdata`
  ADD PRIMARY KEY (`u_id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `filme`
--
ALTER TABLE `filme`
  MODIFY `f_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT für Tabelle `genre`
--
ALTER TABLE `genre`
  MODIFY `g_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT für Tabelle `regisseur`
--
ALTER TABLE `regisseur`
  MODIFY `reg_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT für Tabelle `schauspieler`
--
ALTER TABLE `schauspieler`
  MODIFY `sc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT für Tabelle `userdata`
--
ALTER TABLE `userdata`
  MODIFY `u_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `fgenre`
--
ALTER TABLE `fgenre`
  ADD CONSTRAINT `film` FOREIGN KEY (`f_id`) REFERENCES `filme` (`f_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `genre` FOREIGN KEY (`g_id`) REFERENCES `genre` (`g_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints der Tabelle `regifilme`
--
ALTER TABLE `regifilme`
  ADD CONSTRAINT `regifilme_ibfk_1` FOREIGN KEY (`reg_id`) REFERENCES `regisseur` (`reg_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `regifilme_ibfk_2` FOREIGN KEY (`f_id`) REFERENCES `filme` (`f_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints der Tabelle `scfilme`
--
ALTER TABLE `scfilme`
  ADD CONSTRAINT `scfilme_ibfk_1` FOREIGN KEY (`f_id`) REFERENCES `filme` (`f_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `scfilme_ibfk_2` FOREIGN KEY (`sc_id`) REFERENCES `schauspieler` (`sc_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints der Tabelle `umovie`
--
ALTER TABLE `umovie`
  ADD CONSTRAINT `umovie_ibfk_1` FOREIGN KEY (`f_id`) REFERENCES `filme` (`f_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `umovie_ibfk_2` FOREIGN KEY (`u_id`) REFERENCES `userdata` (`u_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
