import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class database {
	private Connection connect = null;
	private Statement stat = null;
	private ResultSet res = null;
	private PreparedStatement prepStat = null;
	private String user = "root";
	private String password = "";
	private String datName = "webtechproj";
	
	public database(){
		try{
			Class.forName("com.mysql.jdbc.Driver");
			connect = DriverManager.getConnection("jdbc:mysql://localhost/" + datName + "?" + "user="+user+"&password="+password);
			stat = connect.createStatement();
			
			/*
			writeUser("bla","test");
			writeMovie("bla",1991);
			writeGenre("test");
			writeMovieGenre(1,2);
			writeUserMovie(2,1);
			 */
			//getUserMovie(3);
			wipe();
			
			connect.close();
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	public ArrayList getUserMovie(int u_id){
		ArrayList<film> mov = new ArrayList<>();
		ArrayList<ArrayList> ret = new ArrayList<>();

		if(userFunctions.getUserByID(u_id,connect,stat,res)){
			System.out.println("F");
			try {
				res = stat.executeQuery("select * from webtechproj.v_user_movie where u_id='"+u_id+"'");
				while(res.next()){
					mov.add(new film(res.getString("name"),res.getInt("jahr"),res.getString("genre")));
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	public boolean login(String name, String password){
		res = searchUser(name);
		try {
			if(password.equals(res.getString("passwort"))){
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public int getGenrebyMovie(int f_id){
		int ret=0;
		try {
			res = stat.executeQuery("select * from webtechproj.fgenre where f_id='"+f_id+"'");
			ret = res.getInt("g_id");
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return ret;
	}

	public String getGenrebyID(int g_id){
		String ret=null;
		try {
			res = stat.executeQuery("select * from webtechproj.genre where g_id='"+g_id+"'");
			ret = res.getString("name");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ret;
	}

	public ResultSet searchMovieByID(int f_id){
		try {
			res = stat.executeQuery("select * from webtechproj.filme where f_id='"+f_id+"'");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}


	
	public boolean writeUser(String name, String password){
		if(searchUser(name)!=null)
			return false;
		try{
			prepStat = connect.prepareStatement("insert into "+datName+".userdata (name,passwort) values (?,?)");
			prepStat.setString(1, name);
			prepStat.setString(2, password);
			prepStat.execute();
			return true;
		} catch(SQLException e){
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean writeMovie(String name, int year){
		if(searchMovie(name,year)!=null)
			return false;
		try{
			prepStat = connect.prepareStatement("insert into "+datName+".filme (jahr,name) values (?,?)");
			prepStat.setString(2, name);
			prepStat.setInt(1, year);
			prepStat.execute();
			return true;
		} catch(SQLException e){
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean writeGenre(String name){
		if(searchGenre(name)!=null)
			return false;
		try{
			prepStat = connect.prepareStatement("insert into "+datName+".genre (name) values (?)");
			prepStat.setString(1, name);
			prepStat.execute();
			return true;
		} catch(SQLException e){
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean writeMovieGenre(int g_id, int f_id){
		if(searchMovieGenre(g_id,f_id)!=null){
			return false;
		}
		try {
			prepStat = connect.prepareStatement("insert into "+datName+".fgenre (g_id,f_id) values (?,?)");
			prepStat.setInt(1, g_id);
			prepStat.setInt(2, f_id);
			prepStat.execute();
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean writeUserMovie(int f_id, int u_id){
		if(searchUserMovie(f_id,u_id)!=null){
			return false;
		}
		try {
			prepStat = connect.prepareStatement("insert into "+datName+".umovie (f_id,u_id) values (?,?)");
			prepStat.setInt(1, f_id);
			prepStat.setInt(2, u_id);
			prepStat.execute();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public void deleteUser(int u_id){
		try {
			prepStat = connect.prepareStatement("delete from "+datName+".userdata where userdata.u_id = "+u_id);
			prepStat.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void deleteGenre(int g_id){
		try {
			prepStat = connect.prepareStatement("delete from "+datName+".genre where genre.g_id = "+g_id);
			prepStat.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void deleteMovie(int f_id){
		try {
			prepStat = connect.prepareStatement("delete from "+datName+".filme where filme.f_id = "+f_id);
			prepStat.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void deleteUserMovie(int f_id, int u_id){
		try {
			prepStat = connect.prepareStatement("delete from "+datName+".umovie where (umovie.f_id = "+f_id+") and (umovie.u_id="+u_id+")");
			prepStat.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void deleteMovieGenre(int g_id, int f_id){
		try {
			prepStat = connect.prepareStatement("delete from "+datName+".fgenre where (fgenre.g_id = "+g_id+") and (fgenre.f_id="+f_id+")");
			prepStat.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public ResultSet searchMovie(String name, int year){
		try {
			res = stat.executeQuery("select * from webtechproj.filme where (name='"+name+"') AND (jahr="+year+")");
			if(!res.next())
				res=null;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public ResultSet searchUser(String name){
		res=null;
		try {
			res = stat.executeQuery("select * from webtechproj.userdata where name='"+name+"'");
			if(!res.next())
				res=null;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}
	
	public ResultSet searchGenre(String name){
		res = null;
		try {
			res = stat.executeQuery("select * from webtechproj.genre where name='"+name+"'");
			if(!res.next())
				res=null;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}
	
	public ResultSet searchMovieGenre(int g_id, int f_id){
		res = null;
		try{
			res = stat.executeQuery("select * from webtechproj.fgenre where (g_id="+g_id+") and (f_id="+f_id+")");
			if(!res.next())
				res=null;
		}catch (SQLException e){
			e.printStackTrace();
		}
		
		return res;
	}
	
	public ResultSet searchUserMovie(int f_id, int u_id){
		res = null;
		try{
			res = stat.executeQuery("select * from webtechproj.umovie where (f_id="+f_id+") and (u_id="+u_id+")");
			if(!res.next())
				res=null;
		}catch (SQLException e){
			e.printStackTrace();
		}
		return res;
	}

	private void wipe(){
		try {

			prepStat = connect.prepareStatement("delete from "+datName+".fgenre");
			prepStat.execute();

			prepStat = connect.prepareStatement("delete from "+datName+".umovie");
			prepStat.execute();

			prepStat = connect.prepareStatement("delete from "+datName+".filme");
			prepStat.execute();

			prepStat = connect.prepareStatement("alter table webtechproj.filme AUTO_INCREMENT = 1");
			prepStat.execute();

			prepStat = connect.prepareStatement("delete from "+datName+".userdata");
			prepStat.execute();

			prepStat = connect.prepareStatement("alter table webtechproj.userdata AUTO_INCREMENT = 1");
			prepStat.execute();

			prepStat = connect.prepareStatement("delete from "+datName+".genre");
			prepStat.execute();

			prepStat = connect.prepareStatement("alter table webtechproj.genre AUTO_INCREMENT = 1");
			prepStat.execute();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/*
	public ArrayList<film> getUserMovie(int u_id){
		ArrayList<Integer> f_idList = new ArrayList<>();
		ArrayList<film> movies = new ArrayList<>();

		try {
			res = stat.executeQuery("select * from webtechproj.umovie where u_id='"+u_id+"'");
			while(res.next()){
				f_idList.add(res.getInt("f_id"));
				System.out.println(res.getString("f_id"));
			}
			for (int f_id:f_idList) {
				res = searchMovieByID(f_id);
				res.next();
				movies.add(new film(res.getString("name"),res.getInt("jahr"),getGenrebyID(getGenrebyMovie(f_id))));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}


		return movies;
	}
	 */
}