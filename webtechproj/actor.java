public class actor {

    private int sc_id;
    private String forename;
    private String surname;

    public actor(int sc_id, String forename, String surname) {
        setSc_id(sc_id);
        setForename(forename);
        setSurname(surname);
    }

    public int getSc_id() {
        return sc_id;
    }

    public void setSc_id(int sc_id) {
        this.sc_id = sc_id;
    }

    public String getForename() {
        return forename;
    }

    public void setForename(String forename) {
        this.forename = forename;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
}