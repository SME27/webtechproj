/**
 * Created by Blacki on 07.06.2017.
 */
public class regisseur {

    private int reg_id;
    private String forename;
    private String surname;

    public regisseur(int reg_id, String forename, String surname) {
        setReg_id(reg_id);
        setForename(forename);
        setSurname(surname);
    }

    public int getReg_id() {
        return reg_id;
    }

    public void setReg_id(int reg_id) {
        this.reg_id = reg_id;
    }

    public String getForename() {
        return forename;
    }

    public void setForename(String forename) {
        this.forename = forename;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
}
