import java.util.ArrayList;

public class film{
    private String name;
    private int jahr;
    private String genre;
    private int f_id;
    private ArrayList<actor> actors;
    private ArrayList<regisseur> regisseurs;

    public film(String name, int jahr, String genre){
        setName(name);
        setJahr(jahr);
        setGenre(genre);
    }

    public ArrayList<actor> getActors() {
        return actors;
    }

    public void setActors(ArrayList<actor> actors) {
        this.actors = actors;
    }

    public ArrayList<regisseur> getRegisseurs() {
        return regisseurs;
    }

    public void setRegisseurs(ArrayList<regisseur> regisseurs) {
        this.regisseurs = regisseurs;
    }

    public String getGenre(){
        return genre;
    }


    public int getF_id() {
        return f_id;
    }

    public void setF_id(int f_id) {
        this.f_id = f_id;
    }

    private void setGenre(String genre){
        this.genre=genre;
    }

    public String getName() {
        return name;
    }

    private void setName(String name) {
        this.name = name;
    }

    public int getJahr() {
        return jahr;
    }

    private void setJahr(int jahr) {
        this.jahr = jahr;
    }
}